import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import axios from 'axios';
import { RootState } from '../app/store';

export interface ArticlesState {
  articles: Array<any>,
  initialArticles: Array<any>,
  status: 'idle' | 'loading' | 'failed'
}

const initialState: ArticlesState = {
  articles: [],
  initialArticles: [],
  status: 'idle'
}

export const getInitialArticlesAsync = createAsyncThunk(
  'articles/fetchArticles',
  async () => {
    const res = await axios.get('https://newsapi.org/v2/everything', {
      params: {
        apiKey: 'c9ca1b6d416444c1a037d8730db23a2f',
        q: 'bitcoin',
        pageSize: 100,
        page: 1,
      }
    })
    return res.data;
  }
)

export const articlesSlice = createSlice({
  name: 'articles',
  initialState,
  reducers: {
    queryArticles: (state, action: PayloadAction<string>) => {
      state.articles = state.initialArticles.filter(x => {
        return (x.description && x.description.includes(action.payload)) || (x.title && x.title.includes(action.payload))
      })
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(getInitialArticlesAsync.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(getInitialArticlesAsync.fulfilled, (state, action: PayloadAction<any>) => {
        state.status = 'idle';
        const articles = action.payload.articles.map((one: any) => ({
          id: Math.random().toFixed(12).substr(2),
          ...one,
        }))
        state.initialArticles = [...articles];
        state.articles = [...articles];
      });
  }
});

export const { queryArticles } = articlesSlice.actions

export const selectArticles = (state: RootState) => state.articles.articles;

export const selectInitialArticles = (state: RootState) => state.articles.initialArticles;

export default articlesSlice.reducer