import ReactDOM from 'react-dom';
import { ThemeProvider } from '@mui/material/styles';
import { Provider } from 'react-redux';

import App from './App';
import { store } from './app/store';
import theme from './theme';

ReactDOM.render(
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <App />
    </ThemeProvider>
  </Provider>,
  document.querySelector('#root'),
);
