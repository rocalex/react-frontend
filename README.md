# viAct Coding Test (Frontend Developer)

## Requirements

- Create a single application that displays news articles from newsapi.org
- Required framework: React.js, Redux, Typescript, Material UI
- The application must be compatible on desktop, tablets, and mobile devices.

## Install

```
$ npm install
```

## Start

```
$ npm start
```
